package MQS;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.net.Socket;
import java.util.Date;

class ClientHandler extends Thread {
	Socket client;
	int clientNumber;

	public ClientHandler(Socket inClient, int counter) 
	{
		client = inClient;
		clientNumber = counter;
	}

	public void run() 
	{
		try 
		{
			DataInputStream inStream = new DataInputStream(client.getInputStream());
			DataOutputStream outStream = new DataOutputStream(client.getOutputStream());
			String clientMessage = "", serverMessage = "";
			
			File clientFile = new File("..\\..\\Desktop\\Client" + clientNumber + "_1212.txt" );
			clientFile.createNewFile();
			FileWriter fileWriter = new FileWriter("..\\..\\Desktop\\Client" + clientNumber + "_1212.txt");
			
			do
			{
				clientMessage = inStream.readUTF();
				fileWriter.write(clientMessage + " on " + new Date() + "\n");

				System.out.println("From Client-" + clientNumber + ": Message is :" + clientMessage + " on " + new Date());
				serverMessage = "From Server to Client-" + clientNumber + " Message is " + clientMessage + " on " +new Date();
				outStream.writeUTF(serverMessage);
				outStream.flush();
			} while (!clientMessage.equals("quit"));
			
			System.out.println("Server closing... Thank you!!");
			inStream.close();
			outStream.close();
			client.close();
			fileWriter.close();
		} 
		catch (Exception ex) 
		{
			System.out.println("Unexpected error occurred!!");
		}
	}
}
