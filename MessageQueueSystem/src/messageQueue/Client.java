package messageQueue;

import java.net.*;
import java.io.*;

public class Client
{
	private static DataInputStream inStream;
	private static DataOutputStream outStream;
	private static Socket socket;
	private static String host = "127.0.0.1";
	private static int port = 1212;

	public static void main(String[] args) throws IOException 
	{
		Client.getMessage();
		Client.closeConnections();
	}

	public static void getMessage()
	{
		try 
		{
			socket = new Socket(host, port);
			inStream = new DataInputStream(socket.getInputStream());
			outStream = new DataOutputStream(socket.getOutputStream());
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String clientMessage = "", serverMessage = "";
			System.out.println("Enter \"quit\" to quit");
			
			while (!clientMessage.equals("quit"))
			{
				System.out.println("Enter message :)");
				clientMessage = reader.readLine();
				outStream.writeUTF(clientMessage);
				outStream.flush();
				serverMessage = inStream.readUTF();
				System.out.println(serverMessage);
			}
		}
		catch (Exception e) 
		{
			System.out.println("Unexpected error occurred!!");
		}
	}
	
	public static void closeConnections() 
	{
		System.out.println("Server closing.. Thank you!!");
		try 
		{
			socket.close();
			inStream.close();
			outStream.close();
		} 
		catch (Exception e) 
		{
			System.out.println(e);
		}
	}
}
