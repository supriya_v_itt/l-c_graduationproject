package messageQueue;

import java.sql.*;
import java.util.Date;

public class StoreClientData 
{
	int counter = 0, clientNumber = 0;
	
	public void getConnection(Connection connection) 
	{
		try 
		{
			clientNumber += 1;
			String client = "Client" + clientNumber;
			
			PreparedStatement statement = connection.prepareStatement("CREATE TABLE " + client +  "(id integer primary key, Date varchar(50), Data varchar(20) );") ;
			statement.executeUpdate();	
			counter = 0;
	        
	        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO ClientData VALUES (?,?);");
	        preparedStatement.setInt(1, clientNumber);
	        preparedStatement.setString(2, client);
	        preparedStatement.executeUpdate();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
	
	public void storeData(String clientMessage, Connection connection) 
	{
		try 
		{
			String client = "Client" + clientNumber;
			
			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO "+ client +" VALUES (?,?,?);");
			preparedStatement.setInt(1, counter++);
			preparedStatement.setString(2, "" + new Date());
			preparedStatement.setString(3, clientMessage);
			preparedStatement.executeUpdate();
			
		}
		catch (Exception e) { System.out.println(e + "Unexpected error occurred");	}
	}
}
