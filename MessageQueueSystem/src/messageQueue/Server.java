package messageQueue;

import java.net.*;
import java.io.*;

public class Server 
{
	public static ServerSocket server;
	private static int port = 1212;

	public static void main(String[] args) throws IOException 
	{
		Server.getConnection();
		server.close();
	}
	
	public static void getConnection() 
	{
		try 
		{
			server = new ServerSocket(port);
			int counter = 0;
			System.out.println("Server Started ....");
			while (true) 
			{
				Socket client = server.accept();
				counter++;
				System.out.println("Client No:" + counter + " started!");
				ClientHandler clientHandler = new ClientHandler(client, counter);
				clientHandler.start();
			}
		} 
		catch (Exception e) 
		{
			System.out.println("Unexpected error occurred!!");
		}
	}
}