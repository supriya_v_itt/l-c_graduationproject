package messageQueue;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;

class ClientHandler extends Thread 
{
	Socket client;
	int clientNumber;
	Connection connection = null;
	static StoreClientData storeClientData = new StoreClientData();

	public ClientHandler(Socket inClient, int counter) 
	{
		try
		{
			client = inClient;
			clientNumber = counter;
			String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=MessageQueue;user=sa;password=Supp2098";
			DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
			connection = DriverManager.getConnection(connectionUrl);
		}
		catch (Exception ex) 
		{
			System.out.println("Unexpected error occurred!!");
		}
	}

	public void run() 
	{
		try 
		{	
			DataInputStream inStream = new DataInputStream(client.getInputStream());
			DataOutputStream outStream = new DataOutputStream(client.getOutputStream());
			String clientMessage = "", serverMessage = "";
			storeClientData.getConnection(connection);
			
			do
			{
				clientMessage = inStream.readUTF();
				
				System.out.println("From Client-" + clientNumber + ": Message is :" + clientMessage + " on " + new Date());
				serverMessage = "From Server to Client-" + clientNumber + " Message is " + clientMessage + " on " +new Date();
				outStream.writeUTF(serverMessage);
				storeClientData.storeData(clientMessage, connection);

				outStream.flush();
			} while (!clientMessage.equals("quit"));
			
			System.out.println("Server closing... Thank you!!");
			inStream.close();
			outStream.close();
			client.close();
		} 
		catch (Exception ex) 
		{
			System.out.println("Unexpected error occurred!!");
		}
	}
}
